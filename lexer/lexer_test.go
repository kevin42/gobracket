package lexer

import (
	"gitlab.com/kevin42/gobracket/token"
	"testing"
)

func TestNextToken(t *testing.T) {
	input := `(def five 5)
    (def ten 10)
    (defun add [x y]
      (+ x y))
    (add five ten)
    
    (- (/ * 5))
    (< 5 10)
    (> 10 5)
    (cond ((< 5 10) true
           (false)))
    (not true)
    (equal 5 5)`

	tests := []struct {
		expectedType    token.TokenType
		expectedLiteral string
	}{
		{token.LPAREN, "("},
		{token.DEF, "def"},
		{token.IDENT, "five"},
		{token.INT, "5"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.DEF, "def"},
		{token.IDENT, "ten"},
		{token.INT, "10"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.DEFUN, "defun"},
		{token.IDENT, "add"},
		{token.LSQUAR, "["},
		{token.IDENT, "x"},
		{token.IDENT, "y"},
		{token.RSQUAR, "]"},
		{token.LPAREN, "("},
		{token.PLUS, "+"},
		{token.IDENT, "x"},
		{token.IDENT, "y"},
		{token.RPAREN, ")"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.IDENT, "add"},
		{token.IDENT, "five"},
		{token.IDENT, "ten"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.MINUS, "-"},
		{token.LPAREN, "("},
		{token.SLASH, "/"},
		{token.ASTERISK, "*"},
		{token.INT, "5"},
		{token.RPAREN, ")"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.LT, "<"},
		{token.INT, "5"},
		{token.INT, "10"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.GT, ">"},
		{token.INT, "10"},
		{token.INT, "5"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.COND, "cond"},
		{token.LPAREN, "("},
		{token.LPAREN, "("},
		{token.LT, "<"},
		{token.INT, "5"},
		{token.INT, "10"},
		{token.RPAREN, ")"},
		{token.TRUE, "true"},
		{token.LPAREN, "("},
		{token.FALSE, "false"},
		{token.RPAREN, ")"},
		{token.RPAREN, ")"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.NOT, "not"},
		{token.TRUE, "true"},
		{token.RPAREN, ")"},
		{token.LPAREN, "("},
		{token.EQUAL, "equal"},
		{token.INT, "5"},
		{token.INT, "5"},
		{token.RPAREN, ")"},
		{token.EOF, ""},
	}

	l := New(input)

	for i, tt := range tests {
		tok := l.NextToken()

		if tok.Type != tt.expectedType {
			t.Fatalf("tests[%d] - tokenType wrong. expected=%q, got=%q(%q)",
				i, tt.expectedType, tok.Type, tok.Literal)
		}

		if tok.Literal != tt.expectedLiteral {
			t.Fatalf("test[%d] literal wrong. expected=%q, got=%q(%q)",
				i, tt.expectedLiteral, tok.Literal, tok.Type)
		}
	}
}
