package main

import (
	"fmt"
	"gitlab.com/kevin42/gobracket/repl"
	"os"
	"os/user"
)

func main() {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	fmt.Printf("Hello %s! This is the go bracket language!\n", user.Username)
	repl.Start(os.Stdin, os.Stdout)
}
