package token

type TokenType string

type Token struct {
	Type    TokenType
	Literal string
}

const (
	ILLEGAL = "ILLEGAL"
	EOF     = "EOF"

	// Identifiers + literals
	IDENT = "IDENT"
	INT   = "INT"

	// Operators
	PLUS     = "+"
	MINUS    = "-"
	ASTERISK = "*"
	SLASH    = "/"

	LT = "<"
	GT = ">"

	LPAREN = "("
	RPAREN = ")"
	LSQUAR = "["
	RSQUAR = "]"

	// Keywords
	DEF   = "DEF"
	DEFUN = "DEFUN"
	NOT   = "NOT"
	TRUE  = "TRUE"
	FALSE = "FALSE"
	COND  = "COND"
	EQUAL = "EQUAL"
)

var keywords = map[string]TokenType{
	"def":   DEF,
	"defun": DEFUN,
	"not":   NOT,
	"true":  TRUE,
	"false": FALSE,
	"cond":  COND,
	"equal": EQUAL,
}

func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return IDENT
}
